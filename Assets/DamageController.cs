using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DamageController : MonoBehaviour
{
    public Animator anim;
    public enum HitDirection { Left, Right };
    public List<Weapon> ownWeapons;
    // Start is called before the first frame update
    void Start()
    {
        ownWeapons = GetComponentsInChildren<Weapon>().ToList();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Hitted(HitDirection _direction)
    {
        if (_direction == HitDirection.Left)
        {
            anim.Play("ReceiveLeft");
        }
        if (_direction == HitDirection.Right)
        {
            anim.Play("ReceiveRight");
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.GetComponent<Weapon>())
        {
            if (!ownWeapons.Contains(collision.gameObject.GetComponent<Weapon>()))
                Hitted(collision.gameObject.GetComponent<Weapon>().direction);
        }
    }
}
