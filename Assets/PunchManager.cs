using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchManager : MonoBehaviour
{
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    { 
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 mousePos = Input.mousePosition;
            if(mousePos.x < Screen.width / 2)
            {
                Debug.Log("Left click");
                anim.SetTrigger("LeftPunch");
            }
            else if (mousePos.x > Screen.width / 2)
            {
                anim.SetTrigger("RightPunch");
                Debug.Log("Right click");
            }
        }
    }

}
