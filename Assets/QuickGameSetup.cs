using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickGameSetup : MonoBehaviour
{
    public List<Camera> cameras;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < DataManager.main.players.Count; i++)
        {
            cameras[i].gameObject.SetActive(true);
        }
        if (DataManager.main.players.Count == 4)
        {
            SetRect(0, -0.5f, 0.5f);
            SetRect(1, 0.5f, 0.5f);
            SetRect(2, -0.5f, -0.5f);
            SetRect(3, 0.5f, -0.5f);
        }
        if (DataManager.main.players.Count == 3)
        {
            SetRect(0, 0f, 0.5f);
            SetRect(1, 0.5f, -0.5f);
            SetRect(2, -0.5f, -0.5f);
        }
        if (DataManager.main.players.Count == 2)
        {
            SetRect(0, -0.5f, 0);
            SetRect(1, 0.5f, 0);
        }
        if (DataManager.main.players.Count == 1 )
        {
            SetRect(0, 0, 0);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
    void SetRect(int camIndex, float valueX, float valueY)
    {
        cameras[camIndex].rect = new Rect(valueX, valueY, cameras[camIndex].rect.width, cameras[camIndex].rect.height);
    }
}
