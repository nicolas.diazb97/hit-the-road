﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContractsEvents : MonoBehaviour
{
    public Animator White;

    public Animator Entregar;
    public Animator Omitir;

    public GameObject DropDown;
    public GameObject BringUp;

    public void EntregarAnimation()
    {
        Entregar.Play("Grow");
        Omitir.Play("Grow");
    }

    public void BringUpOn()
    {
        DropDown.SetActive(false);
        BringUp.SetActive(true);
    }

    public void DropDownOn()
    {
        DropDown.SetActive(true);
        BringUp.SetActive(false);
    }

    public void BringUpWhite()
    {
        White.Play("Bring-Up");
    }
}
