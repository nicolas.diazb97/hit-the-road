﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSliderEvent : MonoBehaviour
{
    public Animator FirstWhiteBox;

    public void WhiteBoxesAnimationUp()
    {
        FirstWhiteBox.Play("Bring Up");
    }
}
