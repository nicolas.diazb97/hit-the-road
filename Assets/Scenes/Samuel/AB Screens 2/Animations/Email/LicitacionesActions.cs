﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LicitacionesActions : MonoBehaviour
{
    public Animator Licitaciones;

    public GameObject AceptarEvent;

    public void AceptarButtonEvent()
    {
        AceptarEvent.SetActive(true);
    }

    public void DropDown()
    {
        Licitaciones.Play("Expand");
    }

    public void BringUp()
    {
        Licitaciones.Play("Retract");
    }
}
