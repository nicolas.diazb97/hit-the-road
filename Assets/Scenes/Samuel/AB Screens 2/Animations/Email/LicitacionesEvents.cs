﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LicitacionesEvents : MonoBehaviour
{
    public GameObject DropDown;
    public GameObject BringUp;

    public void DropDownEvent()
    {
        DropDown.SetActive(false);
        BringUp.SetActive(true);
    }

    public void BringUpEvent()
    {
        BringUp.SetActive(false);
        DropDown.SetActive(true);
    }
}
