﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OmitirButton : MonoBehaviour
{
    public GameObject popUpOmitir;

    public int contadorOmitir = 0;

    public void FirstContract()
    {
        popUpOmitir.SetActive(true);

        contadorOmitir = 1;
    }

    public void SecondContract()
    {
        popUpOmitir.SetActive(true);

        contadorOmitir = 2;
    }
}
