﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodegaSlider : MonoBehaviour
{
    public Animator Sillas;

    public GameObject DropDown;
    public GameObject BringUp;
    public GameObject WhiteSillas;
    public Animator Linea1;
    public Animator Linea2;


    public void SillasDropDown()
    {
        Sillas.Play("Drop-Down");
        DropDown.SetActive(false);
        BringUp.SetActive(true);
        
    }

    public void SillasBringUp()
    {
        WhiteSillas.SetActive(false);
        Linea1.Play("Linea-sube");
        Linea2.Play("Linea2-sube");
    }
}
