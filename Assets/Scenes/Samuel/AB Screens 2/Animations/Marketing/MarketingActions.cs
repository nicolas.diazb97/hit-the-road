﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketingActions : MonoBehaviour
{
    public GameObject BotonPapel;
    public GameObject BotonRedes;
    public GameObject BotonRadio;

    public GameObject BotonPapel2;
    public GameObject BotonRedes2;
    public GameObject BotonRadio2;

    public Text Titulo;
    public Text Porcentajes;

    public void VolantesPrincipal()
    {
        BotonPapel2.SetActive(true);
        BotonRedes2.SetActive(true);
        BotonRadio.SetActive(true);

        BotonPapel.SetActive(false);
        BotonRedes.SetActive(false);
        BotonRadio2.SetActive(false);

        Titulo.text = "Campaña con volantes impresos";
        Porcentajes.text = "+20% - 40%";
    }

    public void RedesPrincipal()
    {
        BotonRedes.SetActive(true);
        BotonPapel.SetActive(true);
        BotonRadio.SetActive(true);

        BotonPapel2.SetActive(false);
        BotonRedes2.SetActive(false);
        BotonRadio2.SetActive(false);

        Titulo.text = "Campaña en redes sociales";
        Porcentajes.text = "+30% - 50%";
    }

    public void RadioPrincipal()
    {
        BotonRadio2.SetActive(true);
        BotonRedes2.SetActive(true);
        BotonPapel.SetActive(true);

        BotonRadio.SetActive(false);
        BotonPapel2.SetActive(false);
        BotonRedes.SetActive(false);

        Titulo.text = "Campaña con pautas radiales";
        Porcentajes.text = "+40% - 60%";
    }
}
