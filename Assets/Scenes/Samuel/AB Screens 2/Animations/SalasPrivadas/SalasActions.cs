﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SalasActions : MonoBehaviour
{
    public GameObject PopUpSalasSolicitud;
    public GameObject SolicitarAcceso;
    public GameObject Cancelar;
    public GameObject solicitarAccesoGrey2;
    public Text estadoUsers;

    public GameObject solicitarAcceso2;
    public GameObject Cancelar2;
    public GameObject solicitarAccesoGrey1;
    public Text estadoUsers2;

    public GameObject PopUpSalas;

    public int activeCounter = 0;
    public void OpenPopUpSalasSolicitud1()
    {
        activeCounter = 1;
        PopUpSalasSolicitud.SetActive(true);
        Cancelar.SetActive(true);
        SolicitarAcceso.SetActive(false);

        estadoUsers.text = "En solicitud";
        solicitarAccesoGrey2.SetActive(true);

    }

    public void OpenPopUpSalasSolicitud2()
    {
        activeCounter = 2;
        PopUpSalasSolicitud.SetActive(true);
        Cancelar2.SetActive(true);
        solicitarAcceso2.SetActive(false);

        estadoUsers2.text = "En solicitud";
        solicitarAccesoGrey1.SetActive(true);

    }


    public void ClosePopUpSalasSolicitud()
    {
        PopUpSalasSolicitud.SetActive(false);
    }

    public void OpenPopUpSalas()
    {
        PopUpSalas.SetActive(true);
    }

    public void ClosePopUpSalas()
    {
        PopUpSalas.SetActive(false);
    }

    public void ReturnPopUpSalas()
    {
        //Debug.Log(activeCounter);
        if(activeCounter == 1) { 
            PopUpSalas.SetActive(false);
            SolicitarAcceso.SetActive(true);
            Cancelar.SetActive(false);

            estadoUsers.text = "Abierta";
            solicitarAccesoGrey2.SetActive(false);
        }
        else if (activeCounter == 2)
            {
                PopUpSalas.SetActive(false);
                solicitarAcceso2.SetActive(true);
                Cancelar2.SetActive(false);

                estadoUsers2.text = "Abierta";
                solicitarAccesoGrey1.SetActive(false);
            }
        }
    
}
