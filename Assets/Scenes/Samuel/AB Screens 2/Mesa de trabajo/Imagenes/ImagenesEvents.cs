﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImagenesEvents : MonoBehaviour
{
    public Animator MDF;
    public Animator Pino;
    public Animator Guayacan;

    public Animator MDFSilla;
    public Animator PinoSilla;
    public Animator GuayacanSilla;

    public GameObject MDFTorre;
    public GameObject PinoTorre;
    public GameObject GuayacanTorre;

    public GameObject MDFTrompo;
    public GameObject PinoTrompo;
    public GameObject GuayacanTrompo;

    public void GrowMaterialsHoldersSillas()
    {
        MDF.Play("Grow");
        Pino.Play("Grow");
        Guayacan.Play("Grow");
    }

    public void GrowMaterialsHoldersTorres()
    {
        MDF.Play("Grow");
        Pino.Play("Grow");
        Guayacan.Play("Grow");
    }

    public void GrowMaterialsHoldersTrompos()
    {
        MDF.Play("Grow");
        Pino.Play("Grow");
        Guayacan.Play("Grow");
    }

    public void EncenderTorres()
    {
        
    }

    public void EncenderTrompos()
    {
        
    }
}
