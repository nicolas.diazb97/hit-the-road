﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarpenterDownEvent : MonoBehaviour
{
    public Button carpenterButton;
    public Button sastreButton;
    public GameObject pablo;

    public void DeactivateCarpenter()
    {
        carpenterButton.interactable = false;
        sastreButton.interactable = true;
    }

    public void DeactivateTailor()
    {
        carpenterButton.interactable = true;
        sastreButton.interactable = false;
    }
}
