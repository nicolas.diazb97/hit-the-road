﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailorEvent : MonoBehaviour
{
    public GameObject TailorUp;
    public GameObject Tailor;
    public void Event()
    {
        TailorUp.SetActive(true);
        Tailor.SetActive(false);
    }
}
