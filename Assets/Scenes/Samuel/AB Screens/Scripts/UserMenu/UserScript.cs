﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UserScript : MonoBehaviour
{
    public GameObject popUp;


    public void LogIn()
    {
        SceneManager.LoadScene("LogIn");
    }

    public void PopUp()
    {
        popUp.SetActive(true);
    }

    public void Deny()
    {
        popUp.SetActive(false);
    }
    public void Accept()
    {
        SceneManager.LoadScene("ChooseLogged");
    }
}
