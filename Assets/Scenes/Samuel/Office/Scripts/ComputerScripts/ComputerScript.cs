﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerScript : MonoBehaviour
{
    public GameObject popUp;
    public GameObject TableRelated;
    public GameObject ComputerRelated;
    public GameObject CloseThePopUp;
    public GameObject OnComputer;
    public GameObject OffComputer;

    public void PopUp()
    {
        popUp.SetActive(true);
        ComputerRelated.SetActive(true);
        TableRelated.SetActive(false);
        OnComputer.SetActive(true);
    }

    public void ClosePopUp()
    {
        CloseThePopUp.SetActive(false);
        OffComputer.SetActive(false);
    }
}
