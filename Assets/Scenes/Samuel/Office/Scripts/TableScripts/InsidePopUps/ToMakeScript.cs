﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToMakeScript : MonoBehaviour
{
    public GameObject popUp;

    public void ClosePopUp()
    {
        popUp.SetActive(false);
    }
}
