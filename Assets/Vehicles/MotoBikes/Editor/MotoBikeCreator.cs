﻿using UnityEngine;
using UnityEditor;
using System;
using WheelsSystem;

public class MotoBikeCreator : QuadBikeCreator
{
    protected int lineralAbsorberCount;
    protected Transform[] lineralAbsorbers;
    protected LineralAbsorber.Parameters[] lineralAbsorberParameters;

    [MenuItem("GameObject/3D Object/Vehicles/Moto bike")]
    static void Init()
    {
        window = (MotoBikeCreator)EditorWindow.GetWindow(typeof(MotoBikeCreator), true, "Moto bike creator", true);
        ShowWindow();
    }
    protected override void OnEnable ()
    {
        base.OnEnable();
        lineralAbsorberParameters = new LineralAbsorber.Parameters[0];
    }
    protected override void OnDisable ()
    {
        base.OnDisable();
    }
    protected override void SetAbsorbers()
    {
        base.SetAbsorbers();
        EditorGUILayout.LabelField("     ");
        lineralAbsorberCount = EditorGUILayout.IntField("Lineral absorbers count", lineralAbsorberCount);
        if (lineralAbsorbers != null)
        {
            for (int i = 0; i < lineralAbsorbers.Length; i++)
            {
                lineralAbsorbers[i] = EditorGUILayout.ObjectField("  Lineral absorbers", lineralAbsorbers[i], typeof(Transform), true) as Transform;
                if (lineralAbsorbers[i])
                {
                    LineralAbsorber.Parameters lineralAbsorberParameter = lineralAbsorberParameters[i];


                    if (lineralAbsorberParameter.name + "" == "")
                    {
                        lineralAbsorberParameter = new LineralAbsorber.Parameters(lineralAbsorbers[i].name, 1000.0f, 5.0f, 40.0f, 20.0f);
                    }
                    lineralAbsorberParameter.name = lineralAbsorbers[i].name;
                    EditorGUILayout.LabelField("     parameters");
                    lineralAbsorberParameter.force = EditorGUILayout.FloatField("     Force", lineralAbsorberParameter.force);
                    lineralAbsorberParameter.damper = EditorGUILayout.FloatField("     Damper", lineralAbsorberParameter.damper);
                    lineralAbsorberParameter.limit = EditorGUILayout.FloatField("     Limit", lineralAbsorberParameter.limit);
                    lineralAbsorberParameter.mass = EditorGUILayout.FloatField("     Mass", lineralAbsorberParameter.mass);
                    EditorGUILayout.LabelField("");
                    lineralAbsorberParameters[i] = lineralAbsorberParameter;
                }
            }
        }
    }
    protected override void TryToSetOthersFromEditorWindow()
    {
        MotoBike moto = (MotoBike)vehicle;
        moto.TryToSetAngularAbsorbersFromEditorWindow(angularAbsorbers, angularAbsorbersSprings, angularAbsorberParameters);
        moto.TryToSetLineralAbsorbersFromEditorWindow(lineralAbsorbers, lineralAbsorberParameters);
    }
    protected override void EditMasField()
    {
        EditMasField(ref lineralAbsorbers, lineralAbsorberCount);
        EditMasField(ref lineralAbsorberParameters, lineralAbsorberCount);
        base.EditMasField();
    }
}
