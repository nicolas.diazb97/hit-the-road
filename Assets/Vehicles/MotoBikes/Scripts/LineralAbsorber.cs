﻿using UnityEngine;
using System.Collections;
using System;

namespace WheelsSystem
{
    [ExecuteInEditMode]
    [AddComponentMenu("Physics/Wheels System/Details/Lineral absorber")]
    [DisallowMultipleComponent]
    public class LineralAbsorber : Absorber
    {
        [Serializable]
        public struct Parameters
        {
            public string name;
            public float force;
            public float damper;
            public float limit; 
            public float mass;

            public Parameters(string name, float torque, float damper, float limit, float mass)
            {
                this.name = name;
                this.force = torque;
                this.damper = damper;
                this.limit = limit;
                this.mass = mass;
            }
            public static bool operator !=(Parameters p1, Parameters p2)
            {
                return p1.force != p2.force || p1.damper != p2.damper || p1.limit != p2.limit || p1.mass != p2.mass;
            }
            public static bool operator ==(Parameters p1, Parameters p2)
            {
                return p1.force == p2.force && p1.damper == p2.damper && p1.limit == p2.limit && p1.mass == p2.mass;
            }
            public override bool Equals(object obj)
            {
                return base.Equals(obj);
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
            public override string ToString()
            {
                return string.Format("[Parameters]");
            }
        }
        public ConfigurableJoint joint;
        public Parameters parameters;
        private Parameters oldParameters;
        private Transform root;
        private Steering steering;
        private Vector3 jointStartPosition;

        void OnEnable ()
        {
            root = transform.GetComponentInParent<Vehicle>().transform;
            #if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                if (!body)
                {
                    GameObject absorber = new GameObject(name + "_Joint");

                    Transform physics = root.Find("Physics");
                    if (!physics)
                    {
                        physics = new GameObject("Physics").transform;
                        physics.parent = root;
                        physics.position = root.position;
                        physics.transform.localScale = Vector3.one;
                    }
                    absorber.transform.parent = physics;
                    absorber.transform.position = transform.position;
                    absorber.transform.rotation = transform.rotation;
                    joint = absorber.AddComponent<ConfigurableJoint>();
                    body = absorber.GetComponent<Rigidbody>();
                    joint.anchor = Vector3.zero;
                    joint.axis = Vector3.forward;
                    connectedBody = root.GetComponent<Rigidbody>();

                    joint.connectedBody = connectedBody;
                    parameters = new Parameters(name, 30000.0f, 500.0f, 0.1f, 20.0f);

                    joint.xMotion = ConfigurableJointMotion.Limited;
                    joint.yMotion = ConfigurableJointMotion.Locked;
                    joint.zMotion = ConfigurableJointMotion.Locked;
                    joint.angularXMotion = ConfigurableJointMotion.Locked;
                    joint.angularYMotion = ConfigurableJointMotion.Locked;
                    joint.angularZMotion = ConfigurableJointMotion.Locked;

                    joint.linearLimit = new SoftJointLimit(){limit = parameters.limit, bounciness = 0.7f, contactDistance = 0.0f};
                    joint.xDrive = new JointDrive(){positionSpring = parameters.force, positionDamper = parameters.damper, maximumForce = float.MaxValue};
                }
            }
            else
            {
            #endif
                steering = root.GetComponentInChildren<Steering>();
                GameObject lineralAbsorberPivot = new GameObject("LineralAbsorberPivot");
                lineralAbsorberPivot.transform.parent = transform.parent;
                lineralAbsorberPivot.transform.localPosition = transform.localPosition;
                lineralAbsorberPivot.transform.localRotation = transform.localRotation;
                transform.parent = lineralAbsorberPivot.transform;

                jointStartPosition = VectorOperator.getLocalPosition(root, joint.transform.position);
                #if UNITY_EDITOR
            }
                #endif
        }

        #if UNITY_EDITOR
        void OnDestroy ()
        {
            if (!Application.isPlaying)
            {
                if (body)
                {
                    DestroyImmediate(body.gameObject);
                }
            }
        }
        #endif

        #if UNITY_EDITOR
        void Update ()
        {
            if (!Application.isPlaying)
            {
                if (parameters.name != name)
                {
                    parameters = new Parameters(name, parameters.force, parameters.damper, parameters.limit, parameters.mass);
                }
                body.mass = parameters.mass;
                joint.connectedBody = connectedBody;

                if (oldParameters != parameters)
                {
                    oldParameters = parameters;
                    joint.linearLimit = new SoftJointLimit(){limit = parameters.limit, bounciness = 0.7f, contactDistance = 0.0f};
                    joint.xDrive = new JointDrive(){positionSpring = parameters.force, positionDamper = parameters.damper, maximumForce = float.MaxValue};
                }
            }
        }
        #endif
        void LateUpdate ()
        {
            #if UNITY_EDITOR
            if (Application.isPlaying)
            {
            #endif
                if (connectedBody)
                {
                    transform.LookAt(transform.position + body.transform.forward, steering.transform.up);
                    Vector3 jointPosition = VectorOperator.getLocalPosition(root, joint.transform.position);
                    float orient = Vector3.Dot(jointPosition - jointStartPosition, joint.transform.forward) > 0.0f ? 1.0f : -1.0f;
                    float dicplacementZ = orient * Vector3.Distance(jointPosition, jointStartPosition);
                    transform.localPosition = new Vector3(0.0f, 0.0f, dicplacementZ);
                    CallUpdate();
                }
                #if UNITY_EDITOR
            }
                #endif
        }
    }
}