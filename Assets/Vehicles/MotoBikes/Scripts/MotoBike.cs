﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WheelsSystem
{
    [AddComponentMenu("Physics/Wheels System/Vehicles/Moto bike")]
    public class MotoBike : QuadBike 
    {
        [SerializeField] private List<LineralAbsorber.Parameters> lineralAbsorbersParameters;

        [SerializeField] private List<LineralAbsorber> lineralAbsorbers;

        protected override void SetAbsorbers()
        {
            base.SetAbsorbers();
            SetLineralAbsorbers();
        }
            
        protected override void UpdateAbsorbers()
        {
            base.UpdateAbsorbers();
            UpdateLineralAbsorbers();
        }

        void SetLineralAbsorbers()
        {
            lineralAbsorbersParameters = new List<LineralAbsorber.Parameters>(0);
            lineralAbsorbers = new List<LineralAbsorber>(0);
            foreach (var item in transform.GetComponentsInChildren<Transform>())
            {
                if (!item.name.Contains("Joint") && item.name.Contains("LineralAbsorber"))
                {
                    LineralAbsorber lAbsorber = item.gameObject.GetComponent<LineralAbsorber>();
                    if (!lAbsorber)
                    {
                        lAbsorber = item.gameObject.AddComponent<LineralAbsorber>();
                    }
                    lineralAbsorbers.Add(lAbsorber);
                    lineralAbsorbersParameters.Add(lAbsorber.parameters);
                }
            }
        }
        void UpdateLineralAbsorbers()
        {
            if (lineralAbsorbers == null)
            {
                return;
            }
            int i = 0;
            foreach (LineralAbsorber lineralAbsorber in lineralAbsorbers)
            {
                LineralAbsorber.Parameters lAbsorberParameters = lineralAbsorbersParameters[i];
                lineralAbsorber.parameters = lAbsorberParameters;
                if (lAbsorberParameters.name != lineralAbsorber.name)
                {
                    lAbsorberParameters = new LineralAbsorber.Parameters(lineralAbsorber.name, lAbsorberParameters.force, lAbsorberParameters.damper, lAbsorberParameters.limit, lAbsorberParameters.mass);
                    lineralAbsorbersParameters[i] = lAbsorberParameters;
                }
                i++;
            }
        }
        public void TryToSetLineralAbsorbersFromEditorWindow(Transform[] lineralAbsorbers, LineralAbsorber.Parameters[] lineralAbsorberParameters)
        {
            int i = 0;
            if (lineralAbsorbers != null)
            {
                this.lineralAbsorbersParameters = new List<LineralAbsorber.Parameters>(0);
                this.lineralAbsorbers = new List<LineralAbsorber>(0);
                foreach (var item in lineralAbsorbers)
                {
                    LineralAbsorber lAbsorber = item.gameObject.GetComponent<LineralAbsorber>();
                    if (!lAbsorber)
                    {
                        lAbsorber = item.gameObject.AddComponent<LineralAbsorber>();
                    }

                    this.lineralAbsorbers.Add(lAbsorber);
                    this.lineralAbsorbersParameters.Add(lineralAbsorberParameters[i]);
                    i++;
                }
            }
        }
    }
}
