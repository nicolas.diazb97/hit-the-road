﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WheelsSystem
{
    public class MotoDriver : QuadDriver
    {  
        [SerializeField] private float maxTilt = 35.0f;
        [SerializeField] private float minTilt = 5.0f;
        [SerializeField] private float stabilizingForce = 10.0f;
        private Transform rightFootPivot;
        private Transform leftFootPivot;
        private Transform footPivot;
        private Transform rightRayChecker;
        private Transform leftRayChecker;
        private Transform rightKneeTurnPosition;
        private Transform leftKneeTurnPosition;
        private Transform rightKnee;
        private Transform leftKnee;
        private Transform rootPosition;

        private Transform rayChecker;
        private Vector3 rightFootStartPosition;
        private Quaternion rightFootStartRotation;
        private Vector3 leftFootStartPosition;
        private Quaternion leftFootStartRotation;
        private Vector3 vehicleNeedDirection;

        private Vector3 rightKneeStartPosition;
        private Vector3 leftKneeStartPosition;

        private Vector3 rootStartPosition;
        private bool sleepMode;
        private float sleepModeCalculator;
        [SerializeField] private float reverseAngularVelocity = 2.0f;

        protected override void Awake()
        {
            base.Awake();

            sleepModeCalculator = 0.0f;
            if (!driverIK)
            {
                enabled = false;
                return;
            }
            rightFootPivot = driverIK.Find("RightFootPivot");
            leftFootPivot = driverIK.Find("LeftFootPivot");

            rightRayChecker = driver.Find("RightRayChecker");
            leftRayChecker = driver.Find("LefttRayChecker");

            rightKneeTurnPosition = driver.Find("RightKneeTurnPosition");
            leftKneeTurnPosition = driver.Find("LeftKneeTurnPosition");

            rightKnee = driverIK.Find("RightKnee");
            leftKnee = driverIK.Find("LeftKnee");

            rootPosition = driver.Find("RootPosition");
        }
        protected override void Start ()
        {
            base.Start();
            rightFootStartPosition = VectorOperator.getLocalPosition(vehicle.transform, rightFootPivot.position);
            rightFootStartRotation = rightFootPivot.localRotation;
            leftFootStartPosition = VectorOperator.getLocalPosition(vehicle.transform, leftFootPivot.position);
            leftFootStartRotation = leftFootPivot.localRotation;

            if (rightKneeTurnPosition)
            {
                rightKneeStartPosition = VectorOperator.getLocalPosition(driverIK, rightKnee.position);
            }
            if (leftKneeTurnPosition)
            {
                leftKneeStartPosition = VectorOperator.getLocalPosition(driverIK, leftKnee.position);
            }
            if (rootPosition)
            {
                rootStartPosition = transform.localPosition;
            }
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            if (!vehicleController.enabled)
            {
                return;
            }

            if (steering)
            {
                if (vehicle.body.velocity.magnitude > 2.7f)
                {
                    //float upAngle = Vector3.Dot(vehicle.transform.up, Vector3.up);
                    //float upAngleFactor = Mathf.Clamp01( (1.0f / (0.8f - 1.0f)) * Mathf.Abs( upAngle ) - (1.0f / (0.8f - 1.0f)) );

                    float angularVelocity = Vector3.Dot(vehicle.body.angularVelocity, Vector3.up);
                    driftFactor = Mathf.Clamp01( ((vehicleController.turn > 0.0f ? -1.0f : 1.0f) * (angularVelocity / reverseAngularVelocity)));
                    steering.driftFactor = Mathf.Lerp(1.0f, -1.0f,  driftFactor);
                }
                else
                {
                    steering.driftFactor = Mathf.Lerp(steering.driftFactor, 1.0f, 15.0f * Time.fixedDeltaTime);
                }
            }

            if (Vector3.Dot(vehicle.transform.up, Vector3.up) < 0.2f || (vehicle.body.velocity.magnitude > 2.5f && Vector3.Dot(vehicle.body.velocity.normalized, vehicle.transform.forward) < 0.2f))
            {
                return;
            }
            if (!sleepMode)
            {
                if (vehicle.body.velocity.magnitude < 2.5f)
                {
                    sleepModeCalculator += Time.fixedDeltaTime;
                    if (sleepModeCalculator > 1.0f)
                    {
                        sleepMode = true;
                        sleepModeCalculator = 0.0f;
                    }
                }
                else
                {
                    sleepModeCalculator = 0.0f;
                }

            }
            if (sleepMode && vehicle.body.velocity.magnitude < 2.5f)
            {
                rayChecker = steering.rotationUnit >= 0.0f ? leftRayChecker : rightRayChecker;
                footPivot = steering.rotationUnit >= 0.0f ? leftFootPivot : rightFootPivot;
                Vector3 direction = Vector3.Lerp(Vector3.up, steering.rotationUnit >= 0.0f ? -vehicle.transform.right : vehicle.transform.right, minTilt / 90.0f);
                vehicleNeedDirection = Vector3.Lerp(vehicleNeedDirection, direction, 15.0f * Time.fixedDeltaTime);
                Ray ray = new Ray(rayChecker.position, -rayChecker.up);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 1.0f))
                {
                    footPivot.position = Vector3.Lerp(footPivot.position, hit.point, 50.0f * Time.fixedDeltaTime);
                    footPivot.LookAt(footPivot.position + Vector3.ProjectOnPlane(rayChecker.forward, hit.normal).normalized, hit.normal);
                    if (footPivot == rightFootPivot)
                    {
                        leftFootPivot.position = Vector3.Lerp(leftFootPivot.position, VectorOperator.getWordPosition(vehicle.transform, leftFootStartPosition), 10.0f * Time.fixedDeltaTime);
                        leftFootPivot.localRotation = leftFootStartRotation;
                    }
                    else
                    {
                        rightFootPivot.position = Vector3.Lerp(rightFootPivot.position, VectorOperator.getWordPosition(vehicle.transform, rightFootStartPosition), 10.0f * Time.fixedDeltaTime);
                        rightFootPivot.localRotation = rightFootStartRotation;
                    }
                }

                if (leftKnee && rightKnee && rightKneeTurnPosition && leftKneeTurnPosition)
                {
                    rightKnee.position = Vector3.Lerp(rightKnee.position, VectorOperator.getWordPosition(driverIK, rightKneeStartPosition), 10.0f * Time.fixedDeltaTime);
                    leftKnee.position = Vector3.Lerp(leftKnee.position, VectorOperator.getWordPosition(driverIK, leftKneeStartPosition), 10.0f * Time.fixedDeltaTime);
                }
                if (rootPosition)
                {
                    transform.localPosition = Vector3.Lerp(transform.localPosition, rootStartPosition, 10.0f * Time.fixedDeltaTime);
                }
            }
            else
            {
                sleepMode = false;

                rightFootPivot.position = Vector3.Lerp(rightFootPivot.position, VectorOperator.getWordPosition(vehicle.transform, rightFootStartPosition), 10.0f * Time.fixedDeltaTime);
                rightFootPivot.localRotation = rightFootStartRotation;
                leftFootPivot.position = Vector3.Lerp(leftFootPivot.position, VectorOperator.getWordPosition(vehicle.transform, leftFootStartPosition), 10.0f * Time.fixedDeltaTime);
                leftFootPivot.localRotation = leftFootStartRotation;

                Vector3 direction = Vector3.Lerp(Vector3.up, vehicleController.turn >= 0.0f ? -vehicle.transform.right : vehicle.transform.right, maxTilt * Mathf.Abs(vehicleController.turn) / 90.0f);
                vehicleNeedDirection = Vector3.Lerp(vehicleNeedDirection, direction, 10.0f * Time.fixedDeltaTime);
                if (leftKnee && rightKnee && rightKneeTurnPosition && leftKneeTurnPosition)
                {
                    if (vehicleController.turn < -0.1f)
                    {
                        rightKnee.position = Vector3.Lerp(VectorOperator.getWordPosition(driverIK, rightKneeStartPosition), rightKneeTurnPosition.position, -vehicleController.turn);
                        leftKnee.position = Vector3.Lerp(leftKnee.position, VectorOperator.getWordPosition(driverIK, leftKneeStartPosition), 10.0f * Time.fixedDeltaTime);
                    }
                    else if (vehicleController.turn > 0.1f)
                    {
                        leftKnee.position = Vector3.Lerp(VectorOperator.getWordPosition(driverIK, leftKneeStartPosition), leftKneeTurnPosition.position, vehicleController.turn);
                        rightKnee.position = Vector3.Lerp(rightKnee.position, VectorOperator.getWordPosition(driverIK, rightKneeStartPosition), 10.0f * Time.fixedDeltaTime);
                    }
                }
                if (rootPosition)
                {
                    float angle = Mathf.Clamp01(0.5f - (0.5f - Vector3.Dot(vehicle.transform.forward, Vector3.up)));
                    transform.localPosition = Vector3.Lerp(rootStartPosition, VectorOperator.getLocalPosition(driver, rootPosition.position), angle);
                }
            }
            vehicleNeedDirection = vehicleNeedDirection.normalized;
     
            float reverseTorqueNormalized = Vector3.Dot( Vector3.ProjectOnPlane(vehicle.transform.up, vehicleNeedDirection), Vector3.ProjectOnPlane(vehicle.transform.right, Vector3.up).normalized);
            float reverseTorque = stabilizingForce * reverseTorqueNormalized;

            vehicle.body.AddRelativeTorque(vehicle.body.mass * reverseTorque * Vector3.forward, ForceMode.Force);

        }
    }
}

