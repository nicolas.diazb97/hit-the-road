﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class levelLoader2 : MonoBehaviour
{
    public static levelLoader2 instance;

    [SerializeField]
    private GameObject loading_Bar_holder;
    [SerializeField]
    private Image loading_Bar_progress;

    private float progress_value = 1.1f;
    public float progress_multiplier1 = 0.5f;
    public float progress_multiplier2 = 0.07f;

    public float loadleveltime = 2f;

    private void Awake()
    {
        MakeSingleton();
    }
    private void Start()
    {
        StartCoroutine(LaodingSomeLevel());
    }
    private void Update()
    {
        ShowLoadingScreen();
    }

    void MakeSingleton()
    {
        if(instance != null)
        {
            Destroy(gameObject);

        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void LoadLevel(string levelName)
    {
        loading_Bar_holder.SetActive(true);

        progress_value = 0f;

        //Time.timeScale = 0f;

        SceneManager.LoadScene(levelName);
    }
    void ShowLoadingScreen()
    {
        if (progress_value < 1f)
        {
            progress_value += progress_multiplier1 * progress_multiplier2;
            loading_Bar_progress.fillAmount = progress_value;


            //the loading bar has fnished
            if(progress_value >= 1f)
            {
                progress_value = 1.1f;

                loading_Bar_progress.fillAmount = 0f;

                loading_Bar_holder.SetActive(false);

                //Time.timeScale = 1f;
            }
        }// iff progress value < 1
    }

   /* IEnumerator LaodingSomeLevel()
    {
        yield return new WaitForSeconds(loadleveltime);

        LoadLevel("Menu");
    }*/
    IEnumerator LaodingSomeLevel()
    {
        yield return new WaitForSeconds(loadleveltime);

        LoadLevel("game");
    }
}
