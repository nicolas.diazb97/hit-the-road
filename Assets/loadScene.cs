﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loadScene : MonoBehaviour
{
    private bool oneshot;
    public AudioClip song;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!oneshot)
            {
                AudioSource.PlayClipAtPoint(song, Vector3.zero);
                Invoke("LoadScene", 2.5f);
               oneshot = true;
            }
        }
    }
    void LoadScene()
    {
        Application.LoadLevel("load-scene");
    }
}
